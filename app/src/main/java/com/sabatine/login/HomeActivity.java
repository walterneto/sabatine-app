package com.sabatine.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sabatine.login.dao.UserDAO;
import com.sabatine.login.model.User;
import com.sabatine.login.storage.UsersRepository;

public class HomeActivity extends AppCompatActivity {

    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Recupera usuário logado

        sharedPref = getApplicationContext().getSharedPreferences(
                "session", Context.MODE_PRIVATE);

        User user = new UserDAO(getApplicationContext()).authenticate(sharedPref.getString("email",null),sharedPref.getString("password",null));

        // Atualiza textView com nome do usuario logado.
        TextView nameLabel = (TextView)findViewById(R.id.nameLbl);

        if( nameLabel != null ) {
            if( user != null ) {
                nameLabel.setText(user.getName());
            }
        }
    }

    public void logout(View view) {
        sharedPref = getApplicationContext().getSharedPreferences(
                "session", Context.MODE_PRIVATE);

        // Zera valores
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("password", null);
        editor.putString("email", null);
        editor.commit();

        // Volta para home
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }
}
