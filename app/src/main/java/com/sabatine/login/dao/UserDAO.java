package com.sabatine.login.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.sabatine.login.model.User;

/**
 * Created by Walter on 09/05/2017.
 */
public class UserDAO extends DAO {
    static final String COLUMN_NAME_NAME = "name";
    static final String COLUMN_NAME_EMAIL = "email";
    static final String COLUMN_NAME_PASSWORD = "password";
    static final String TABLE_NAME = "users";
    Database database;

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    " _id INTEGER PRIMARY KEY," +
                    COLUMN_NAME_NAME + " TEXT" + "," +
                    COLUMN_NAME_EMAIL + " TEXT" + "," +
                    COLUMN_NAME_PASSWORD + " TEXT" + " )";

    public UserDAO(Context context){
        database = new Database(context);
    }

    public User insert(User user){
        SQLiteDatabase db = database.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values;
        values = new ContentValues();
        values.put(COLUMN_NAME_NAME, user.getName());
        values.put(COLUMN_NAME_EMAIL, user.getEmail());
        values.put(COLUMN_NAME_PASSWORD,user.getPassword());


        // Insert the new row, returning the primary key value of the new row
        user.setId(db.insert(TABLE_NAME, null, values));
        return user;
    }

    public User authenticate(String email, String password) {
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor c = db.rawQuery("select * from " + TABLE_NAME + " where email = '" + email + "' AND password = '"+ password +"'", null);

        if( ((c != null) && (c.getCount() > 0)) )
            return toModel(c);

        return null;
    }

    public User toModel(Cursor c){
        c.moveToFirst();
        return new User(c.getLong(c.getColumnIndex("_id")),c.getString(c.getColumnIndex("name")), c.getString(c.getColumnIndex("password")), c.getString(c.getColumnIndex("email")));
    }
}
