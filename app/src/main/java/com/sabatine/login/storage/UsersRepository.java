package com.sabatine.login.storage;

import android.util.Log;

import com.sabatine.login.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Walter on 05/04/2017.
 */
public class UsersRepository {
    private static UsersRepository ourInstance = new UsersRepository();
    private List<User> users = new ArrayList<User>();
    private User loggedUser = null;

    public static UsersRepository getInstance() {
        return ourInstance;
    }

    public User getUserByEmail(String email) {
        // Procura usuário pelo email e o retorna.
        for( User u : users ) {
            if( u.getEmail().equals(email) ) {
                return u;
            }
        }
        return null;
    }

    public boolean saveUser(User user) {
        if( getUserByEmail(user.getEmail()) == null ) {
            return users.add(user);
        }
        // Email já cadastrado
        return false;
    }

    public void setLoggedUser(User user) {
        loggedUser = user;
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    private UsersRepository() {
    }
}
