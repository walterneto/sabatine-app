package com.sabatine.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.sabatine.login.dao.UserDAO;
import com.sabatine.login.model.User;
import com.sabatine.login.storage.UsersRepository;

public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Se já há ID logado, inicia atividade Home
        UserDAO userDAO = new UserDAO(getApplicationContext());
        sharedPref = getApplicationContext().getSharedPreferences(
                "session", Context.MODE_PRIVATE);
        if( userDAO.authenticate(sharedPref.getString("email",null),sharedPref.getString("password",null)) != null ) {
            Intent i = new Intent(this, HomeActivity.class);
            startActivity(i);
        }

    }

    public void tryLogin(View view) {
        Context context = getApplicationContext();

        sharedPref = context.getSharedPreferences(
                "session", Context.MODE_PRIVATE);

        UserDAO userDAO = new UserDAO(getApplicationContext());

        String email   = ((EditText)findViewById(R.id.emailTxt)).getText().toString();
        String pwd   = ((EditText)findViewById(R.id.pwdTxt)).getText().toString();


        User user = userDAO.authenticate(email,pwd);
        if ( user != null ) {
            Toast.makeText(context, user.getName()+", seja bem vindo!", Toast.LENGTH_SHORT).show();

            // Salva Sessão
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("email", user.getEmail());
            editor.putString("password", user.getPassword());
            editor.commit();

            // Inicia Home
            Intent i = new Intent(this, HomeActivity.class);
            startActivity(i);
        } else {
            Toast.makeText(context, "Credenciais incorretas", Toast.LENGTH_SHORT).show();
        }
    }

    public void newRegister(View view) {
        Intent i = new Intent(this, RegisterActivity.class);
        startActivity(i);
    }
}
