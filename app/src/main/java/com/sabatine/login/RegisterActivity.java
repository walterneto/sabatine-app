package com.sabatine.login;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sabatine.login.dao.UserDAO;
import com.sabatine.login.model.User;
import com.sabatine.login.storage.UsersRepository;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void saveAccount(View view) {
        Context context = getApplicationContext();

        String email   = ((EditText)findViewById(R.id.emailTxt)).getText().toString();
        String name   = ((EditText)findViewById(R.id.nameTxt)).getText().toString();
        String pwd   = ((EditText)findViewById(R.id.pwdTxt)).getText().toString();

        if( email.isEmpty() || name.isEmpty() || pwd.isEmpty() ) {
            Toast.makeText(context, "Preencha todos os dados", Toast.LENGTH_SHORT).show();
            return;
        }

        UserDAO userDAO = new UserDAO(getApplicationContext());
        User user = userDAO.insert(new User(name,pwd,email));

        if( user.getId() > 0 ) {
            Toast.makeText(context, "Conta criada com sucesso", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(context, "Conta já cadastrada", Toast.LENGTH_SHORT).show();
        }

    }
}
